<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\article;

/**
 * MySearch represents the model behind the search form about `common\models\article`.
 */
class MySearch extends article
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'status', 'view', 'is_top', 'is_hot', 'is_best', 'user_id', 'deleted_at', 'favourite', 'published_at', 'created_at', 'updated_at'], 'integer'],
            [['title', 'category', 'description', 'source', 'module'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = article::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'status' => $this->status,
            'view' => $this->view,
            'is_top' => $this->is_top,
            'is_hot' => $this->is_hot,
            'is_best' => $this->is_best,
            'user_id' => $this->user_id,
            'deleted_at' => $this->deleted_at,
            'favourite' => $this->favourite,
            'published_at' => $this->published_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'module', $this->module]);

        return $dataProvider;
    }
}
