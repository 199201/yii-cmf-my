<?php

namespace backend\controllers;

use rbac\models\Menu;
//use backend\widgets\Menu;
use Yii;
use common\models\TestDemo;
use common\models\TestDemoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use backend\widgets\ToChineseNumber;

/**
 * TestDemoController implements the CRUD actions for TestDemo model.
 */
class TestDemoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TestDemo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TestDemoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /* test */
        //$res = new ToChineseNumber( 1892600 ); //$res->arr(4,'');
        //var_dump($res->arr(4,''));
        $a = $this->hideStr('huangwei', 2, 4);
        //var_dump($a);
        $time = $this->countDown('2017-08-12', '2018-01-01');
        var_dump($time);
        $getIp = $this->getIP();
        //var_dump($getIp);
        /********************* 递归 *************************/
        $data = Menu::find()->orderBy('id asc')->asArray()->all();
        //var_dump($data);
        $res = $this->limitTree($data);

        /* test */

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'res' => $res,
        ]);
    }

    /**
     * Displays a single TestDemo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TestDemo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TestDemo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TestDemo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TestDemo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TestDemo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TestDemo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TestDemo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 隐藏字符串的部分字符
     * @param $str    待处理的字符串
     * @param $start  开始的位置
     * @param $show   显示几个字符
     * @return mixed
     */
    protected function hideStr($str, $start, $show)
    {

        $length = strlen($str); //先求长度
        $left = $length - $show;
        //str_repeat(string,repeat) 函数把字符串重复指定的次数。
        $star = str_repeat('*', $left);    //要替换的字符串,是一串*
        //substr_replace(string,replacement,start,length) 函数把字符串的一部分替换为另一个字符串。
        $str2 = substr_replace($str, $star, $start, $left);  //开始替换整个字符串
        return $str2;

    }

    /**
     * 倒计时判断
     * @param $beginTime 定义开始时间
     * @param $endTime  定义结束时间
     * @return string
     */
    protected function countDown($beginTime, $endTime)
    {
        $time = time();
        $endTime = strtotime($endTime);//时间戳
        $beginTime = strtotime($beginTime);
        if ($time < $beginTime) {
            $leftStr = $beginTime - $time;//据开始时间。。。
            $txt = '距离开始时间还有 ';
        } else if ($time > $endTime) {
            $leftStr = $endTime - $time;//已结束。。。
            $txt = '已结束 ';
        } else {
            $leftStr = $time - $beginTime;//已过去。。。
            $txt = '时间已过去 ';
        }
        $r = '';
        if ($leftStr >= 365 * 24 * 60 * 60) {
            $resYear = floor($leftStr / (365 * 24 * 60 * 60));
            $leftStr = $leftStr % (365 * 24 * 60 * 60);
            $r = $resYear . '年';
        }
        if ($leftStr >= 24 * 60 * 60) {
            $resDay = floor($leftStr / (24 * 60 * 60));
            $leftStr = $leftStr % (24 * 60 * 60);
            $r .= $resDay . '天';
        }
        if ($leftStr >= 60 * 60) {
            $resHour = floor($leftStr / (60 * 60));
            $leftStr = $leftStr % (60 * 60);
            $r .= $resHour . '小时';
        }
        if ($leftStr >= 60) {
            $resMinute = floor($leftStr / 60);
            $leftStr = $leftStr % 60;
            $r .= $resMinute . '分钟';
        }
        $r .= $leftStr . '秒';
        $res = $txt . $r;

        return $res;
    }

    /**
     * 获取ip
     * @return string
     */
    protected function getIP()
    {
        if (getenv('HTTP_CLIENT_IP')) {# 检查客户端IP
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {#  检查系统IP
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {#  检查IP
            $ip = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {#  检查IP
            $ip = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {#  检查IP
            $ip = getenv('HTTP_FORWARDED');
        } else {# 检查系统内置IP
            $ip = $_SERVER['REMOTE_ADDR'];
        }#返回IP

        return $ip;

    }

    /**
     * 初始化 $treeList
     * @var array
     */
    static public $treeList = array();

    /**
     * 递归处理
     * @param $data
     * @param null $parent
     * @param int $level
     * @return array
     */
    protected function limitTree($data, $parent = null, $level = 1)
    {
        foreach ($data as $key => &$vo) {
            if ($vo['parent'] == $parent) {
                $vo['level'] = $level;
                self::$treeList[] = $vo;
                unset($data[$key]);
                self::limitTree($data, $vo['id'], $level + 1);
            }
        }
        return self::$treeList;
    }
}
