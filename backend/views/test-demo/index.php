<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginBlock('content-header') ?>
<? //= $this->title . ' ' . Html::a('Create Article', ['create'], ['class' => 'btn btn-primary btn-flat btn-xs']) ?>
<?php $this->endBlock() ?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="box box-primary">
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                'name',
                'parent',
                'order',
                'icon',
                // 'view',
                // 'is_top',
                // 'is_hot',
                // 'is_best',
                // 'description',
                // 'user_id',
                // 'source',
                // 'deleted_at',
                // 'favourite',
                // 'published_at',
                // 'created_at',
                // 'updated_at',
                // 'module',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>

<!--*************************************************************************************************-->
<!--测试用-->

<!--<div>
    <table>
        <th>
            <tr>
                <td>名称</td>
            </tr>
        </th>
        <tbody >
        <?php /*foreach ($res as $k => $v) { */?>
            <tr>
                <td style="text-indent:<?/*= $v['level'] * 20 */?>px;">
                    <?/*= $v['level'] != 1 ? '| -- ' : '' */?>
                    <?/*= $v['name'] */?>
                </td>
            </tr>
        <?php /*} */?>

        </tbody>
    </table>
</div>-->

