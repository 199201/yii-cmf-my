<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'category',
            'category_id',
            'status',
            'view',
            'is_top',
            'is_hot',
            'is_best',
            'description',
            'user_id',
            'source',
            'deleted_at',
            'favourite',
            'published_at',
            'created_at',
            'updated_at',
            'module',
        ],
    ]) ?>
    </div>
</div>
